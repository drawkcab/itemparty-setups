# itemparty-setups

This repository contains setups for [ITEM.PARTY](https://item.party/) contributed by the community.

You can browse the contributed setups [using ITEM.PARTY](https://item.party/s/https(3A(2F(2Fdrawkcab.gitlab.io(2Fitemparty-setups(2Fsetups.s.json/:/i/:.s.json/:/view) and start using them from there.

## Requesting a setup

Create an issue in this repository describing your goal and the kind of items you have, and I may be able to make a setup for you.

## Contributing a setup

Create a merge request in this repo with
1. your setup file at `public/<YOUR-USERNAME>/<FILENAME>` directory
2. an update to `/setups.json` with `"filename": <FILENAME>`, `"creator": <YOUR-USERNAME>` and a `name` field matching the `layout.title` in the setup file